using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanonRotation : MonoBehaviour
{
    public Vector3 _maxRotation;
    public Vector3 _minRotation;
    private float offset = -51.6f;
    public GameObject ShootPoint;
    public GameObject Bullet;
    public float ProjectileSpeed = 0;
    public float MaxSpeed;
    public float MinSpeed;
    public GameObject PotencyBar;
    private float initialScaleX;

    public Camera mainCamera;
    public Vector3 mousePosition;

    private void Awake()
    {
        initialScaleX = PotencyBar.transform.localScale.x;
    }

    private void Start()
    {
        mainCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
    }

    void Update()
    {
        mousePosition = mainCamera.ScreenToWorldPoint(Input.mousePosition);

        Vector3 rotation = mousePosition - transform.position;
        var ang = (Mathf.Atan2(rotation.y, rotation.x) * 180f / Mathf.PI + offset);
        transform.rotation = Quaternion.Euler(0, 0, ang);

        if (Input.GetMouseButton(0))
        {
            ProjectileSpeed += Time.deltaTime * 20;
        }
        if (Input.GetMouseButtonUp(0))
        {
            var projectile = Instantiate(Bullet, ShootPoint.transform.position, Quaternion.identity);
            projectile.GetComponent<Rigidbody2D>().velocity = rotation.normalized * ProjectileSpeed;
            ProjectileSpeed = 0f;
        }
        CalculateBarScale();

    }
    public void CalculateBarScale()
    {
        PotencyBar.transform.localScale = new Vector3(Mathf.Lerp(0, initialScaleX, ProjectileSpeed / 5 / MaxSpeed),
            transform.localScale.y,
            transform.localScale.z);
    }
}
